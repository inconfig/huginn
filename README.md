# Header1

## Installation

1) Предварительно необходимо уставновить salt stack, mongodb-server, pymongo
так же необходимо проверить наличие библиотек: smtplib, logging.

2) Скачиваем код проекта с gitlab.com
    $ git clone git@gitlab.com:inconfig/huginn.git

## Настройка

### Настройка salt-master

1) Создаем каталог /srv/salt
    $ mkdir -p /srv/salt

2) Делее необходимо скопировать сценарии из проекта, они находятся в каталоге srv
    $ cp -r ./srv/salt /srv/salt

3) Скопировать конфигурацию для salt-master в каталог /etc/salt/master.d/ из каталога env_configs/salt/master
    $ cp env_configs/salt/master/* /etc/salt/master.d/

4) Создать каталог для конфигурации сервиса huginn
    $ mkdir -p /etc/huginn/ ; touch /etc/huginn/huginn.conf

5) Задать конфигурацию в huginn.conf
    router_protocol: tcp 
    router_address: 127.0.0.1
    router_port: 27070
    workers: 3

6) Создать каталог с конфигурациями модулей и каталог с шаблонами jinja2
    $ mkdir -p /srv/huginn/config/modules/
    $ mkdir -p /srv/huginn/templates/mail
Конфугирация и примеры шаблонов находяться в каталоге srv проекта

7) Переместить salt/runners_scripts/huginnmq.py в /opt/salt/exctensions/runners

8) Перезапустить salt-master
    $ systemctl restart salt-master

9) Запустить huginn, скрипт для запуска: cli/daemons/huginn.py




