#!/usr/bin/python
# encoding: utf-8

from __future__ import absolute_import

# Import python libs
import pymongo
from pymongo import MongoClient

# Import huginn libs
from config import module_config
from logs import logging
from render import get_template

logger = logging.getLogger(__name__)

VALID_OPTS = {
    'db_server': str,
    'port': str,
}

__virtual_name__ = 'changes'


def _get_config():
    config = module_config(__virtual_name__, VALID_OPTS)

    if config is None:
        logger.error('Load {0} module configuration failed'.format(__virtual_name__))

    return config


def _init_client(server, port):
    try:
        client = MongoClient(server, port)
    except Exception as err:
        logger.error('Error {0} when mongodb init client'.format(err))
        client = None
    return client


def _get_database(client, server_id):
    try:
        database = client[server_id]
    except Exception as err:
        logger.error('Error {0} when get servers database'.format(err))
        database = None

    return database


def _get_collection(database):
    # Get changes collection
    try:
        collection = database['changes']
    except Exception as err:
        logger.error('Error {0} when get collection changes form {1}'.format(err, database))
        collection = None

    return collection


# Update functions
def _update(collection, data):
    retcode = True
    try:
        ins_id = collection.insert_one(data).inserted_id
        logger.info('Document {0} insterted id {1}'.format(data, ins_id))
    except Exception as err:
        logger.error('Error {0} when update {1}'.format(err, collection))
        retcode = False

    return retcode


def update(data, id):
    config = _get_config()

    client = _init_client(config['db_server'], config['port'])
    if client is None:
        logger.error('MongoDB client is None return False')
        return False

    database = _get_database(client, id.split('.')[0])
    if database is None:
        logger.error('MongoDB database is None return False')
        return False

    collection = _get_collection(database)
    if collection is None:
        logger.error('MongoDB colection is None return False')
        return False

    retcode = _update(collection, data)

    return retcode
