#!/usr/bin/python
# encoding: utf-8

from __future__ import absolute_import

# Import python libs
import smtplib
from email.mime.text import MIMEText

# Import huginn libs
from config import module_config
from logs import logging
from render import get_template

logger = logging.getLogger(__name__)

VALID_OPTS = {
    'relay_server': str,
    'from': str,
    'template_dir': str,
}

__virtual_name__ = 'mail'


def _get_config():
    config = module_config(__virtual_name__, VALID_OPTS)

    if config is None:
        logger.error('Load {0} module configuration failed'.format(__virtual_name__))

    return config


def _init_client(server):
    try:
        return smtplib.SMTP(server)
    except Exception as err:
        logger.error('Error {0} when init smtp client'.format(err))
        return None


def _get_msg(content, subject, mail_from, mail_to):
    try:
        msg = MIMEText(content)
    except Exception as err:
        logger.error('Error {0} when get mail content'.format(err))
        msg = None

    if msg is None:
        return None

    msg['Subject'] = subject
    msg['From'] = mail_from
    msg['To'] = mail_to

    return msg


def _send(client, msg):
    retcode = True
    try:
        client.sendmail(msg['from'], [msg['to']], msg.as_string())
    except Exception as err:
        logger.error('Error {0} when send email'.format(err))
        retcode = False

    return retcode


def email(template_name, subject, to, context):
    config = _get_config()

    email_content = get_template(config['template_dir'], template_name, context)
    if email_content is None:
        return False

    message = _get_msg(email_content, subject, config['from'], to)
    if message is None:
        return False

    logging.debug(message)
    client = _init_client(config['relay_server'])
    if client is None:
        return False

    if _send(client, message):
        return True
    else:
        return False
