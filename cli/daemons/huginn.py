#!/usr/bin/python
# encoding: utf-8

# Import python libs
import os
import sys
from time import sleep
from multiprocessing import Process

# Append sys.path
sys.path.append('/opt/dev/huginn')

# Import huginn libs
import mq
from mq import ioloop
from config import daemon_config
from signalhandlers import SignalHandler

# Setup logger
from logs import logging

logger = logging.getLogger(__name__)


def terminate_router(protocol, address, port, workers):
    context = mq.zmq.Context()
    socket = context.socket(mq.zmq.PUSH)
    socket.connect('{0}://{1}:{2}'.format(protocol, address, port))
    socket.send('Exit:{0}'.format(workers), mq.zmq.NOBLOCK)


def router(protocol, address, port):
    # Create signalhandler
    logger.info('Init huginn router. PID:{0}'.format(os.getpid()))

    try:
        router = ioloop.IOLoopMqRouter(protocol, address, port)
    except Exception as err:
        logger.error('Error {0} when init IOLoopMqRouter object'.format(err))


def worker():
    # Create signalhandler
    sighandler = SignalHandler()

    logger.info('Init worker process with PID:{0}'.format(os.getppid()))

    try:
        ioloop.IOLoopMqClient()
    except Exception as err:
        logger.error('Error {0} when init IOLoopMqClent'.format(err))


def _main():
    # Create sighandler
    sighandler = SignalHandler()

    # Child process list
    procs = []

    logger.info('Start huginn daemon')

    opts = daemon_config()

    # Init Router process
    logger.debug('Start router process')
    proc = Process(target=router, args=(opts['router_protocol'], opts['router_address'], opts['router_port']))
    proc.daemon = True
    proc.start()
    procs.append(proc)
    logger.debug('Router process init finish. PID{0}'.format(proc.pid))

    # Init workers
    for proc in xrange(opts['workers']):
        logger.debug('Start worker process {0}'.format(proc))
        proc = Process(target=worker)
        proc.daemon = True
        proc.start()
        procs.append(proc)
        logger.debug('Woker process start with PID:{0}'.format(proc.pid))

    # Init main loop 
    while not sighandler.kill:
        sleep(1)

    # Terminate process when SIGTERM received
    logger.debug('{0} SIGTERM recived'.format(os.getpid()))
    terminate_router(opts['router_protocol'], opts['router_address'], opts['router_port'], opts['workers'])
    for proc in procs:
        proc.terminate()

    # Wait procs returncode
    for proc in procs:
        proc.join()

    return True


if __name__ == '__main__':
    exit_code = _main()

    if exit_code:
        exit_code = 0
    else:
        exit_code = 1

    sys.exit(exit_code)
