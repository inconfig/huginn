#!/usr/bin/python
# encoding: utf-8

import sys

sys.path.append('/opt/dev/huginn')

import zmq
import mq
from mq import ioloop


def push_client(msg):
    context = zmq.Context()
    client = mq.MQ(context, zmq.PUSH, False, 'tcp', '127.0.0.1', 27070)

    client.socket.send_string(msg, zmq.NOBLOCK)


if __name__ == '__main__':
    push_client(str(sys.argv[1]))
