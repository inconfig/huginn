#!/usr/bin/python
# encoding: utf-8

import logging

logging.basicConfig(format='%(filename)s[LINE:%(lineno)d]#%(levelname)-8s,[%(asctime)s]%(message)s',
                    level=logging.DEBUG)
