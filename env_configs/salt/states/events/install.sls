huginn/salt/system/new/report:
    event.send:
        - data:
            hostname: {{grains['host']}}
            os: {{grains['os']}}
            productname: {{grains['productname']}}
            serial: {{grains['serialnumber']}}
            memory: {{grains['mem_total']}}

