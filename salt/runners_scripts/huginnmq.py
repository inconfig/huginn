#!/usr/bin/python
# encoding: utf-8

# This module must be save at
# /usr/lib/python2.7/site-packages/salt/runners/

from __future__ import absolute_import

# Import python libs
import logging
import zmq

log = logging.getLevelName(__name__)

__func_alias__ = {
    'send_': 'send'
}


def _init_socket(protocol, address, port):
    zmq_context = zmq.Context()
    socket = zmq_context.socket(zmq.PUSH)
    try:
        socket.connect('{0}://{1}:{2}'.format(protocol, address, port))
    except Exception as err:
        log.error('Error {0} when connect to huginn socket')
        socket = None

    return socket


def _get_data(message, func, func_args):
    data = {}
    data.update({'message': message, 'func': func, 'args': func_args})

    return data


def send_(message, func, func_args, protocol, address, port):
    retcode = True
    data = _get_data(message, func, func_args)
    socket = _init_socket(protocol, address, port)
    if socket is not None:
        try:
            socket.send_json(data)
        except Exception as err:
            log.error('Error {0} when send message'.format(err))
            retcode = False

    return retcode
