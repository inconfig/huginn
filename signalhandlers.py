#!/usr/bin/python
# encoding: utf-8

# Import python libs
import signal


class SignalHandler(object):
    kill = False
    restart = False

    def __init__(self):
        signal.signal(signal.SIGTERM, self.exit_gracefuly)
        signal.signal(signal.SIGHUP, self.restart_gracefuly)

    def exit_gracefuly(self, signum, frame):
        self.kill = True

    def restart_gracefuly(self, signum, frame):
        self.restart = True
