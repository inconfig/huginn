#!/usr/bin/python
# encoding: utf-8

# Import python libs
import os
import json

# Import zmq ioloop
from zmq.eventloop import ioloop, zmqstream

# Get logger
from logs import logging

logger = logging.getLogger(__name__)

# Import huginn libs
import mq
from loader import HuginnLoader


class IOLoopMQ(object):
    '''
        IOLoop MQ class, for async processing messages
    '''

    def __init__(self, protocol, address, port=None):
        # Init sighandler
        super(IOLoopMQ, self).__init__()
        # Get variables from input args
        self.protocol = protocol
        self.address = address
        self.port = port

        # Install tornado ioloop
        ioloop.install()

        # init pull socket stream
        self.init_pull()

    def init_pull(self, bind=True):
        self.pull_context = mq.zmq.Context()

        # Init pull socket
        self.pull = mq.MQ(self.pull_context,
                          mq.zmq.PULL,
                          bind,
                          self.protocol,
                          self.address,
                          self.port)

        # Register pull socket stream
        self.pull_stream = zmqstream.ZMQStream(self.pull.socket)
        self.pull_stream.on_recv(self.get_message)

    def get_message(self, msg):
        logger.debug('PID[{0}]:Message {1} received from socket. Type {2}'.format(os.getpid(), msg, type(msg)))

        if msg[0] == 'Exit':
            self.terminate()

    def terminate(self):
        rcode = True
        logger.info('Stop IOLoop instance {0}'.format(os.getpid()))
        try:
            ioloop.IOLoop.instance().stop()
        except Exception as err:
            logger.error('Error {0} when stoping IOLopp instance'.format(err))
            rcode = False
        return rcode


class IOLoopMqRouter(IOLoopMQ, object):
    '''
        Class for main process. Receive messages from huginn salt-runner and send to workers. 
    '''
    d_protocol = 'ipc'
    d_address = '/var/run/huginn.ipc'

    def __init__(self, protocol, address, port):
        super(IOLoopMqRouter, self).__init__(protocol, address, port)

        # Init push socket
        self._init_push()

        # Start ioloop instance
        ioloop.IOLoop.instance().start()

    def _init_push(self):
        self.push_context = mq.zmq.Context()

        # Init push socket
        self.push = mq.MQ(self.push_context,
                          mq.zmq.PUSH,
                          True,
                          self.d_protocol,
                          self.d_address)

    def get_message(self, msg):
        logger.debug('PID[{0}]:Message {1} received from socket.'.format(os.getpid(), msg))

        if msg[0].split(':')[0] == 'Exit':
            for i in xrange(int(msg[0].split(':')[1])):
                try:
                    self.push.socket.send_string('Exit', mq.zmq.NOBLOCK)
                except Exception as err:
                    logger.error('PID[{0}]:Error {1} when send message.'.format(os.getpid(), err))
            self.terminate()
        else:
            try:
                self.push.socket.send(msg.pop(), mq.zmq.NOBLOCK)
            except Exception as err:
                logger.error('PID[{0}]:Error {1} when send message.'.format(os.getpid(), err))


class IOLoopMqClient(IOLoopMQ, object):
    '''
        Class for worker process.
    '''
    protocol = 'ipc'
    address = '/var/run/huginn.ipc'
    port = None

    def __init__(self):
        self.init_pull(bind=False)

        # Start ioloop instance
        ioloop.IOLoop.instance().start()

    def get_message(self, msg):
        IOLoopMQ.get_message(self, msg)

        logger.debug('Load json from string')
        try:
            msg = json.loads(msg.pop())
            logger.debug('Json data {0} is loaded. Data type {1}'.format(msg, type(msg)))
        except Exception as err:
            logger.error('Error {0} when loading msg to json format'.format(err))

        # Load function
        try:
            loader = HuginnLoader(msg['func'], msg['args'])
        except Exception as err:
            logger.error('Error {0} when load function'.format(err))

        # Get loaded function
        try:
            function = loader.return_function()
        except Exception as err:
            logger.error('Error {0} when execute function'.format(err))

        # Execute function
        if function is None:
            logger.error('Function {0} not available'.format(msg['func']))
        else:
            try:
                retcode = function(**msg['args'])
                logger.info('Execute function {0} return {1}'.format(msg['func'], retcode))
            except Exception as err:
                logger.error('Error {0} when exceute function {1}'.format(err, function))
