#!/usr/bin/python
# encoding: utf-8

# Get logger
from logs import  logging
logger = logging.getLogger(__name__)

# Import ZeroMQ
try:
    import zmq
    HAS_ZMQ = True
except ImportError as zmq_import_err:
    HAS_ZMQ = False
    
if not HAS_ZMQ:
    logger.error('Missing zmq, please install pyzmq module.')
    
class MQ(object):
    '''
        Base class for create 0mq sockets
    '''
    def __init__(self, zmq_context, socket_type, bind, protocol, address, port=None):
        '''
            Create zmq socket, select communication method(bind or connect)
        '''
        self.z_context = zmq_context
        
        # Create socket, with passed type
        try:
            self.socket = self.z_context.socket(socket_type)
        except Exception as err:
            logger.error('Error {0} when create socket'.format(err))
            
        # Select communication method
        if bind: self.c_method = self.socket.bind
        else: self.c_method = self.socket.connect
        
        # Communicate, bind or connect to created socket
        self._communicate(protocol, address, port)
        
    def _communicate(self, protocol, address, port=None):
        if port is None: uri = '{0}://{1}'
        else: uri = '{0}://{1}:{2}'
        
        try:
            self.c_method(uri.format(protocol, address, port))
        except Exception as err:
            logger.error('Error {0} when communicate'.format(err))
        