#!/usr/bin/python
# encoding: utf-8

from __future__ import absolute_import
from importlib import import_module

# Import hugin libs
from logs import logging

logger = logging.getLogger(__name__)


class HuginnLoader(object):
    '''
        Function loader for processing messages on arived
    '''
    modules = 'exmodules'

    def __init__(self, func_name, func_args):
        logging.debug('Get Huginn excecution modules loader')

        self.module_name = func_name.split('.')[0]
        self.func_name = func_name.split('.')[1]

        self._load_module()
        self._get_function()
        logger.debug('Function load {0}'.format(self.func))

    def _load_module(self):
        try:
            self.module = import_module('{pkg}.{module}'.format(pkg=self.modules, module=self.module_name))
        except Exception as err:
            logger.debug('Error {0} when import module {1}'.format(err, self.module_name))

    def _get_function(self):
        try:
            self.func = getattr(self.module, self.func_name)
        except Exception as err:
            logger.debug(
                'Error {0} when get function {1} from module {2}'.format(err, self.func_name, self.module_name))
            self.func = None

    def return_function(self):
        if self.func is not None:
            return self.func
        else:
            return None
