#!/usr/bin/python
# encoding: utf-8

from __future__ import absolute_import

# Import python libs
from jinja2 import Environment, FileSystemLoader

# Import huginn libs
from logs import logging

logger = logging.getLogger(__name__)


def _get_env(path):
    try:
        j_env = Environment(loader=FileSystemLoader(path))
    except Exception as err:
        logger.error('Error {0} when _get jinja2 enviroment'.format(err))
        j_env = None
    return j_env


def _render(template, context):
    logging.debug(context)
    try:
        rendered_template = template.render(context=context)
    except Exception as err:
        logger.error('Error {0} when render template {1} with context {2}'.format(err, template, context))
        rendered_template = None
    return rendered_template


def get_template(dir, file, context, *args, **kwargs):
    j_env = _get_env(dir)

    if j_env is None:
        logger.error('Jinja env init failed')
        return None

    try:
        template = j_env.get_template('{0}.j2'.format(file))
    except Exception as err:
        logger.error('Error {0} when get template'.format(err))
        return None

    template = _render(template, context)
    if template is None:
        logger.error('Templatate rendering failed')
        return None

    else:
        return template
