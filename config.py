#!/usr/bin/python
# encoding: utf-8

# Import python libs
import six

import yaml

# import huginn libs
# Get logger
from logs import logging

logger = logging.getLogger(__name__)

# Configure yaml
try:
    yaml.Loader = yaml.CLoader
    yaml.Dumper = yaml.CDumper
except Exception as err:
    logger.debug('Error {0} when configure yaml'.format(err))

VALID_OPTS = {
    # Router options
    'router_protocol': str,
    'router_address': str,
    'router_port': str,
    # Workers count
    'workers': int,
}


def _validate_opts(opts):
    errors = []

    err = ('Key \'{0}\' with value {1} has an invalid type of {2}, a {3} is '
           'required for this value')

    key_err = ('Unknown key \'{0}\'')

    for key, value in six.iteritems(opts):
        if key in VALID_OPTS:
            if isinstance(value, VALID_OPTS[key]):
                continue

            if hasattr(VALID_OPTS[key], '__call__'):
                try:
                    VALID_OPTS[key](value)
                    if isinstance(value, (list, dict)):
                        errors.append(
                            err.format(key,
                                       value,
                                       type(value).__name__,
                                       VALID_OPTS[key].__name__
                                       )
                        )
                except (TypeError, ValueError):
                    errors.append(
                        err.format(key,
                                   value,
                                   type(value).__name__,
                                   VALID_OPTS[key].__name__
                                   )
                    )
        else:
            errors.append(
                key_err.format(key)
            )
    for error in errors:
        logger.error(error)
    if errors:
        return False
    return True


def _read_file(path='/etc/huginn/huginn.conf'):
    with open(path, 'r') as fd:
        try:
            config = yaml.load(fd)
        except Exception as err:
            logger.error('Error {0} when read configuration file'.format(err))
            config = None
    return config


def daemon_config():
    logger.debug('Read configuration')
    __opts__ = _read_file()
    if _validate_opts(__opts__):
        return __opts__
    else:
        return None


# Configuration function for execution modules

def _validate_mod_opts(opts, valid_opts):
    VALID_MOD_OPTS = valid_opts

    errors = []

    err = ('Key \'{0}\' with value {1} has an invalid type of {2}, a {3} is '
           'required for this value')

    key_err = ('Unknown key \'{0}\'')

    for key, value in six.iteritems(opts):
        if key in VALID_MOD_OPTS:
            if isinstance(value, VALID_MOD_OPTS[key]):
                continue

            if hasattr(VALID_MOD_OPTS[key], '__call__'):
                try:
                    VALID_MOD_OPTS[key](value)
                    if isinstance(value, (list, dict)):
                        errors.append(
                            err.format(key,
                                       value,
                                       type(value).__name__,
                                       VALID_MOD_OPTS[key].__name__
                                       )
                        )
                except (TypeError, ValueError):
                    errors.append(
                        err.format(key,
                                   value,
                                   type(value).__name__,
                                   VALID_MOD_OPTS[key].__name__
                                   )
                    )
        else:
            errors.append(
                key_err.format(key)
            )
    for error in errors:
        logger.error(error)
    if errors:
        return False
    return True


def module_config(module, valid_opts, path='/srv/huginn/config/modules'):
    opts = _read_file('{0}/{1}.yaml'.format(path, module))

    if not _validate_mod_opts(opts, valid_opts):
        return None

    return opts
