invoke_orchestrate_file:
    runner.state.orchestrate:
       - mods: orch.huginn_send
       - pillar: 
           data: {{ data['data']|json()}}
